# Debian Stretch Mailcow Reverse Proxy for `auto.*.*` domains

Ansible Playbook to configure a Debian Stretch server as a Nginx reverse proxy to allow seperate Let's Encrypt certs for `autoconfig.*` and `autodiscover.*` domains from the main [Mailcow](https://mailcow.email/) domain.

See [this Mailcow issue](https://github.com/mailcow/mailcow-dockerized/issues/461) for the background discussion and thoughts behind this.

To mitigate the potential for requests using HTTP Authentication to `autodiscover.*` domains being used for brute force attacks fail2ban is used.

To configure a Webarchitects server run the [Ansible](https://git.coop/webarch/ansible) Playbook and a Sudoers one and then this one:

```bash
export DISTRO="stretch"
export SERVERNAME="config.webarch.email"
ansible-playbook autoconfig.yml -i ${SERVERNAME}, -e "hostname=${SERVERNAME} distro=${DISTRO}"
```

To add a domain to the server first ensure that the Bind zone for for the new domain contains:

```
autodiscover         IN CNAME         config.webarch.email.
autoconfig           IN CNAME         config.webarch.email.
```

Then SSH into the server and run as root:

```bash
add-domain example.org

```

This will generate the Nginx configuration for the reverse proxy and get a Let's Encrypt certificate.

There is also a script to remove a domain:

```bash
rm-domain example.org

```

## Testing

Requests for things other than `autoconfig.*` and `autodiscover.*` are
redirected back to the front page of the site as we don't want to reverse proxy
these things, for example: 

```
curl -I https://autoconfig.webarch.co.uk/SOGo
```

And:

```
curl -I https://autoconfig.webarch.co.uk/admin.php
```

### Autoconfig

This should result in the following files being available, for example for the <code>webarch.co.uk</code> domain:

* `config-v1.1.xml` [HTTP](http://autoconfig.webarch.co.uk/.well-known/autoconfig/mail/config-v1.1.xml) | [HTTPS](https://autoconfig.webarch.co.uk/.well-known/autoconfig/mail/config-v1.1.xml)

Test the autoconfig redirect to HTTPS:

```
curl -I http://autoconfig.webarch.co.uk/.well-known/autoconfig/mail/config-v1.1.xml
```

Test the autoconfig file headers:

```
curl -I https://autoconfig.webarch.co.uk/.well-known/autoconfig/mail/config-v1.1.xml
```

Test the autoconfig file:

```
curl https://autoconfig.webarch.co.uk/.well-known/autoconfig/mail/config-v1.1.xml
```

### Autodiscover
 
* `autodiscover.xml` [HTTP](http://autodiscover.webarch.co.uk/autodiscover/autodiscover.xml) | [HTTPS](https://autodiscover.webarch.co.uk/autodiscover/autodiscover.xml)


Check the redirect to HTTPS:

```
curl -I http://autodiscover.webarch.co.uk/autodiscover/autodiscover.xml 
```

Check with a uppercase A: 

```
curl -I http://autodiscover.webarch.co.uk/Autodiscover/autodiscover.xml 
```

Check the headers returned with a GET:

```
curl -I https://autodiscover.webarch.co.uk/autodiscover/autodiscover.xml 
```

Check the headers returned with a uppercase A:

```
curl -I https://autodiscover.webarch.co.uk/autodiscover/autodiscover.xml 
```

Check the headers with a username and password:

```
curl -I -u "example@example.org:PASSWORD" https://autodiscover.webarch.co.uk/autodiscover/autodiscover.xml
```

Check the body with a HTTP POST:

```
curl --data @post.xml -u "example@example.org:PASSWORD" https://autodiscover.webarch.co.uk/autodiscover/autodiscover.xml
```

Example `post.xml` file:

```xml
<Autodiscover
  xmlns="http://schemas.microsoft.com/exchange/autodiscover/outlook/requestschema/2006">
  <Request>
    <EMailAddress>example@example.org</EMailAddress>
    <AcceptableResponseSchema>
      http://schemas.microsoft.com/exchange/autodiscover/outlook/responseschema/2006a
    </AcceptableResponseSchema>
  </Request>
</Autodiscover>
```

### fail2ban

Tail the logs on the Mailcow server:

```bash
docker-compose logs -f fail2ban-mailcow
```

Tail the logs on the reverse proxy server:

```bash
tail -f /var/log/nginx/fail2ban.log /var/log/fail2ban.log
```

List the iptables riles on the reverse proxy:

```bash
iptables -L -n
```

## TODO

* The script to add domains could be implemented in Ansible.
